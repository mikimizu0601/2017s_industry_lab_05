package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        Animal[] animals = new Animal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        Animal dog = new Dog();
        Animal bird = new Bird();
        Animal horse = new Horse();
        animals[0] = dog;
        animals[1] = bird;
        animals[2] = horse;

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(Animal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.
        for (int i=0; i<=2; i++){
            String name = list[i].myName();
            String hello = list[i].sayHello();
            System.out.println(name + " says " + hello + ".");
            if (list[i].isMammal()) {
                System.out.println(name + " is a mammal.");
            } else {
                System.out.println(name + " is not a mammal.");
            }
            int legs = list[i].legCount();
            System.out.println("Did I forget to tell you that I have " + legs + " legs.");
            if (list[i] instanceof IFamous){
                IFamous other = (IFamous) list[i];
                String famousName = other.famous();
                System.out.println("This is a famous name of my animal type: " + famousName);
            }
        }
    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
